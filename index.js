const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const bodyParser = require('body-parser');
const keys = require('./config/keys');
require('./models/User');
require('./models/Survey');
require('./services/passport'); // we require this file here just to get it executed, it does not return anything


mongoose.connect(keys.mongoURI);


const app = express();

app.use(bodyParser.json());
app.use(
    cookieSession({
        maxAge: 30 * 24 * 60 * 1000, // this is in miliseconds
        keys: [keys.cookieKey] // this will be used to sign or encrypt our cookie
    })
);
// make passport to use cookies
app.use(passport.initialize());
app.use(passport.session());

//authRoutes file returns a function and we imediately call  that function with app object
require('./routes/authRoutes')(app); 
require('./routes/billingRoutes')(app); // this returns a function , so this 'require' becomes a function where we imediately call with Express app object
require('./routes/surveyRoutes')(app);

if (process.env.NODE_ENV === 'production') {
    // Express will server up production assets like main.js or main.css file
    app.use(express.static('client/build')); // look in to client/build directory

    // express will serve up index.html file if it does not recognize the route
    const path = require('path');
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

const PORT = process.env.PORT || 5000;
console.log(process.env.PORT);
app.listen(PORT);