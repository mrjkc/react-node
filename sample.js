// Write a function to retrieve a blob of json, make an ajax request, we will use 'fetch' function.
// fetch returns a promise which returns an object

// traditional fetch request
/*function fetchAlbums() {
    fetch('http://rallycoding.herokuapp.com/api/music_albums')
        .then(res => res.json())
        .then(json => console.log(json));
}*/

//fetchAlbums();

// Same in ES7 working with promises is easier. New function is Async Await

const  fetchAlbums = async () => {
    const res = await fetch('http://rallycoding.herokuapp.com/api/music_albums')
    const json = await res.json()

    console.log(json);
}

fetchAlbums();