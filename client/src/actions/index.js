import axios from 'axios';
import { FETCH_USER } from './types';

export const fetchUser = () => async dispatch => {  // we dont have return an action here straight away, we dont want to dispatch an action until API call completed
    const res = await axios.get('/api/current_user')
    
    dispatch({ type: FETCH_USER, payload: res.data }); // res is the output from axios
};

export const handleToken = (token) => async dispatch => {
    const res = await axios.post('/api/stripe', token); // when this completes that means we made succesful post request to our backen server

    dispatch({ type: FETCH_USER, payload: res.data });
};


