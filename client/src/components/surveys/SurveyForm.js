// Survey Form shows a form for a user to add input
import _ from 'lodash'; // this gives helper map method
import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form'; // Field component we can use to show any type of different html element that will collect input from a user
import { Link } from 'react-router-dom';
import SurveyField from './SurveyField';
import validateEmails from '../../utils/validateEmails';

const FIELDS = [
    { label: 'Survey Title', name: 'title' },
    { label: 'Subject Line', name: 'subject' },
    { label: 'Email Body', name: 'body' },
    { label: 'Recipient List', name: 'emails' }
];

class SurveyForm extends Component {

    renderFields() {
        
        return _.map(FIELDS, ({ label, name }) => {
            return (
            <Field 
                key={name} 
                component={SurveyField} 
                type="text" 
                label={label} 
                name={name} 
            />
            );
        });
        /*return (
            <div>
                <Field label="Survey Title" type="text" name="title" component={SurveyField} />
                <Field label="Subject Line" type="text" name="subject" component={SurveyField} />
                <Field label="Email Body" type="text" name="body" component={SurveyField} />
                <Field label="Recipient List" type="text" name="emails" component={SurveyField} />

            </div>
        );*/
    }

    render() {
        return ( // handleSubmit is provided by the redux form helper
            <div>
                <form onSubmit={this.props.handleSubmit(values => console.log(values))}> 
                    {/*<Field type="text" name="surveyTitle" component="input" />*/}
                    {this.renderFields()}
                    <Link to="/surveys" className="red btn-flat white-text">
                        Cancel
                    </Link>
                    <button type="submit" className="teal  btn-flat right white-text">
                        Next
                        <i className="material-icons right">done</i>
                    </button>
                </form>
            </div>
        );
    }

}

function validate(values) {
    const errors = {};

    errors.emails = validateEmails(values.emails || '');

    _.each(FIELDS, ({ name }) => {
        if (!values[name]) {
            errors[name] = 'You must provide a value';
        }
    });

    return errors; // if no errors that means that form values are valid
}

export default reduxForm({
    validate, // ES6 here to replace reduc function validate: validate
    form: 'surveyForm'
})(SurveyForm); 