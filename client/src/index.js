import 'materialize-css/dist/css/materialize.min.css';
import './styles/styles.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider} from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';

import App from './components/App';
import reducers from './reducers';

// development only axios helper
import axios from 'axios';
window.axios = axios;

const store = createStore(reducers, {}, applyMiddleware(reduxThunk)); // we ahve to use reducer in the store, we will have auth reducer and surveys reducer

ReactDOM.render(
    <Provider store={store}><App /></Provider>, 
    document.querySelector('#root')
);

// console.log('STRIPE KEY IS', process.env.REACT_APP_STRIPE_KEY);
// console.log('Environement is', process.env.NODE_ENV);;