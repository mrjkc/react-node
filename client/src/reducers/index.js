import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import authReducer from './authReducer';

export default combineReducers({
   auth: authReducer,
   form: reduxForm // this is where all of redux-form state is stored
});