const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin'); // make sure user is authenticated
const requireCredits = require('../middlewares/requireCredits');
const Mailer = require('../services/Mailer');
const surveyTemplate = require('../services/emailTemplates/surveyTemplate');

const Survey = mongoose.model('surveys'); // we arequiring this file not directly to avoid node.js issue
// make sure user has enough credits

module.exports = app => {

    app.get('/api/surveys/thanks', (req, res) => {
        res.send('Thanks for voting!');
    });

    app.post('/api/surveys', requireLogin, requireCredits, async (req, res) => {
        const { title, subject, body, recipients } = req.body; // we are pulling all these properties from the re.body

        const survey = new Survey({
            title, // same as title: title
            subject,
            body,
            recipients: recipients.split(',').map(email => ({ email })), //  same as    recipients: recipients.split(',').map(email => { return { email: email }})
            _user: req.user.id,
            dateSent: Date.now()
        });

        // great place to send email
        const mailer = new Mailer(survey, surveyTemplate(survey));

        try {
            await mailer.send(); // after success we take the survey we created and call. save on this, this is async function we will have to wait for the API to comlpete, the mailer paused on this function
            await surey.save();
            req.user.credits -= 1;
            const user = await req.user.save();

            res.send(user);
        } catch (err) {
            res.status(422);
        }
    });
};

//test