const passport = require('passport');

//we need to export this in order for index.js to be able to use the app.get
module.exports = app => {
  app.get(
    '/auth/google',
    passport.authenticate('google', {
      scope: ['profile', 'email']
    })
  );

  /*app.get('/', (req,res) => {
    res.send({ hello: 'world'});
  });*/

  app.get(
    '/auth/google/callback', 
    passport.authenticate('google'),
    (req, res) => { // we will redirect who is making request to some other route of our application
      res.redirect('/surveys');
    }
  );

  app.get('/api/logout', (req, res) => {
    req.logout();
    res.redirect('/');
  });

  app.get('/api/current_user', (req, res) => {
    res.send(req.user);
  });


};
