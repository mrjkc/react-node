const keys = require('../config/keys');
const stripe = require('stripe')(keys.stripeSecretKey);
const requireLogin = require('../middlewares/requireLogin');
// Stripe token ends up here  and now we should process it
module.exports = app => {
    app.post('/api/stripe', requireLogin, async (req, res) => { // we are not calling require login, we are passing a reference to a function

        const charge = await stripe.charges.create({
            amount: 500,
            currency: 'usd',
            description: '$5 for 5 credits',
            source: req.body.id
        });

        req.user.credits += 5;
        const user = await req.user.save(); // we put await in front as it might take some time to comeplete

        res.send(user); // send back the data that we want to communicate to the browser
    });
};