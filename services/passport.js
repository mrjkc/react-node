
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const keys = require('../config/keys');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id)
    .then(user => {
      done(null, user);
    });
});

passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleClientID,
      clientSecret: keys.googleClientSecret,
      callbackURL: '/auth/google/callback',
      proxy: true 
    },

    async (accessToken, refreshToken, profile, done) => { // this function its whats returned from google, we can add this to database etc.
      const existingUser =  await User.findOne({ googleId: profile.id }) // promise being create here

      if (existingUser) {
        // we already have a record with the given id
        return done(null, existingUser);
      } 
      
      const user = await new User({ googleId: profile.id }).save() // promise being create here
      done(null, user);
    }
  )
);
